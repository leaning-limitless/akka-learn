/****************************************************
 * 创建人：@author fengxin    
 * 创建时间: 2019/7/27/13:30
 * 项目名称: AkkaLearn
 * 文件名称: ConfigLearn.java
 * 文件描述: @Description: config 使用学习
 * 公司名称: 深圳市赢和信息技术有限公司
 * All rights Reserved, Designed By 深圳市赢和信息技术有限公司
 * @Copyright:2019-2019
 *
 ********************************************************/


package com.example.config;

/**
 * 包名称：com.example.config
 * 类名称：ConfigLearn
 * 类描述：config 使用学习
 * 创建人：@author fengxin
 * 创建时间：2019/7/27/13:30
 */

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

public class ConfigLearn {
    public static void main(String[] args){
        Config conf = ConfigFactory.load();
        int bar1 = conf.getInt("akka.foo.bar");
        Config foo = conf.getConfig("akka.foo");
        int bar2 = foo.getInt("bar");
        System.out.println(bar1);
        System.out.println(bar2);
    }
}
