/****************************************************
 * 创建人：@author fengxin    
 * 创建时间: 2019/7/11/16:03
 * 项目名称: AkkaLearn
 * 文件名称: IotMain.java
 * 文件描述: @Description: Iot start
 * 公司名称: 深圳市赢和信息技术有限公司
 * All rights Reserved, Designed By 深圳市赢和信息技术有限公司
 * @Copyright:2019-2019
 *
 ********************************************************/


package com.example.supervisor;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;

import java.io.IOException;

/**
 * 包名称：com.example.createfirstactor
 * 类名称：IotMain
 * 类描述：Iot start
 * 创建人：@author fengxin
 * 创建时间：2019/7/11/16:03
 */


public class IotMain {



    public static void main(String[] args) throws IOException {
        ActorSystem system = ActorSystem.create("iot-system");

        try {
            // Create top level supervisor
            ActorRef supervisor = system.actorOf(IotSupervisor.props(), "iot-supervisor");

            System.out.println("Press ENTER to exit the system");
            System.in.read();
        } finally {
            system.terminate();
        }
    }
}
